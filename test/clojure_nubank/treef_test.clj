(ns clojure-nubank.treef-test
  (:require [clojure.test :refer :all]
            [clojure-nubank.treef :as treef]))

(defn wrap-setup
  [f]
  (println "wrapping setup")
  (treef/reset)
  (f))    

(use-fixtures :once wrap-setup)

(deftest tree-test
  (testing "Test tree search not found"
    (is (nil? (treef/search 100))))

  (testing "Test tree insert"
    (treef/insert 1 10)
    (is (= (count (treef/build-map)) 2)))

  (testing "Test tree retrieve"
    (is (= (treef/get-confirmed 10) false))
    (is (= (count (treef/build-map)) 2)))

  (testing "Test tree insert nested and retrieve nested with points"
    (treef/insert 10 100)
    (is (= (treef/get-points 100) 0))
    (is (= (treef/get-points 10) 0))
    (is (= (treef/get-confirmed 10) true))
    (is (= (count (treef/build-map)) 3)))

  (testing "Test tree insert existing nested and retrieve existing nested with points"
    (treef/insert 10 100)
    (is (= (treef/get-points 100) 0))
    (is (= (treef/get-points 10) 0))
    (is (= (treef/get-confirmed 10) true))
    (is (= (count (treef/build-map)) 3)))

  (testing "Test tree insert nested nested and retrieve nested nested with points"
    (treef/insert 100 1000)
    (is (= (treef/get-points 100) 0))
    (is (= (treef/get-points 10) 1.0))
    (is (= (treef/get-confirmed 100) true))
    (is (= (count (treef/build-map)) 4)))

  (testing "Test tree insert nested nested nested and retrieve nested nested nested with points"
    (treef/insert 1000 10000)
    (is (= (treef/get-points 1000) 0))
    (is (= (treef/get-points 100) 1.0))
    (is (= (treef/get-points 10) 1.5))
    (is (= (treef/get-confirmed 1000) true))
    (is (= (count (treef/build-map)) 5)))

  (testing "Testing confirm method"
    (treef/insert 1000 10001)
    (is (= (treef/get-confirmed 10001) false))
    (treef/confirm 10001 10002)
    (is (= (treef/get-confirmed 10001) true))
    (is (= (treef/get-points 1000) 1.0))
    (is (= (treef/get-points 100) 1.5))
    (is (= (treef/get-points 10) 1.75))))