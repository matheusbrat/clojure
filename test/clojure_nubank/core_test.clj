(ns clojure-nubank.core-test
  (:require [clojure.test :refer :all]
            [clojure-nubank.treef :as treef]
            [clojure-nubank.core :refer [parse-line load-content]]))

(defn wrap-setup
  [f]
  (println "wrapping setup")
  (treef/reset)
  (f))    

(use-fixtures :once wrap-setup)

(deftest tree-test
  (testing "Insert 2 new itens (inexistent, inexistent)"
    (treef/insert 1 2)
    (is (= (count (treef/build-map)) 2))
    (is (= (treef/get-points 1) 0))
    (is (= (treef/get-points 2) 0))
    (is (= (treef/get-confirmed 1) true))
    (is (= (treef/get-confirmed 2) false)))

  (testing "Insert existing itens (existent, existent)"
    (treef/insert 1 2)
    (is (= (count (treef/build-map)) 2))
    (is (= (treef/get-points 1) 0))
    (is (= (treef/get-points 2) 0))
    (is (= (treef/get-confirmed 1) true))
    (is (= (treef/get-confirmed 2) false)))

  (testing "Insert 1 new item (existent, inexistent)"
    (treef/insert 1 3)
    (is (= (count (treef/build-map)) 3))
    (is (= (treef/get-points 1) 0))
    (is (= (treef/get-points 2) 0))
    (is (= (treef/get-points 3) 0))
    (is (= (treef/get-confirmed 1) true))
    (is (= (treef/get-confirmed 2) false))
    (is (= (treef/get-confirmed 3) false)))

  (testing "Insert 1 new item (existent, inexistent)"
    (treef/insert 3 4)
    (is (= (count (treef/build-map)) 4))
    (is (= (treef/get-points 1) 1.0))
    (is (= (treef/get-points 2) 0))
    (is (= (treef/get-points 3) 0))
    (is (= (treef/get-points 4) 0))
    (is (= (treef/get-confirmed 1) true))
    (is (= (treef/get-confirmed 2) false))
    (is (= (treef/get-confirmed 3) true)))

  (testing "Confirm 1 item"
    (treef/confirm 2 4)
    (is (= (treef/get-points 1) 2.0))
    (is (= (treef/get-confirmed 2) true)))

  (testing "Confirm 1 item already confirmed should not change points"
    (treef/confirm 2 4)
    (is (= (treef/get-points 1) 2.0))
    (is (= (treef/get-confirmed 2) true)))  

  (testing "insert parsing line (existent, inexistent)"
    (parse-line "4 5")
    (is (= (treef/get-points 1) 2.5))
    (is (= (count (treef/build-map)) 5)))

  (testing "insert parsing line (existent, inexistent)"
    (parse-line "4 6")
    (is (= (treef/get-points 1) 2.5))
    (is (= (count (treef/build-map)) 6)))

  (testing "inserting already existent and already invited (existent, existent)"
    (treef/insert 2 4)
    (is (= (treef/get-points 1) 2.5))
    (is (= (count (treef/build-map)) 6)))

  (testing "inserting new customer inviting already invited (inexistent, existent)"
    (treef/insert 7 4)
    (is (= (treef/get-confirmed 7) true))
    (is (= (treef/get-points 1) 2.5))
    (is (= (count (treef/build-map)) 7)))

  (testing "reset and import file"
    (treef/reset)
    (load-content "input.txt")
    (is (= (count (treef/build-map)) 6))))