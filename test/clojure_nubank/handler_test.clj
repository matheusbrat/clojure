(ns clojure-nubank.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [clojure-nubank.treef :as treef]
            [clojure-nubank.handler :refer :all]
            [cheshire.core :as json]))

(defn wrap-setup
  [f]
  (println "wrapping setup")
  (treef/reset)
  (f))

(use-fixtures :once wrap-setup)

(deftest handler-test
  (testing "get points endpoint"
    (let [response (app (request :get "/customers/"))]
      (is (= (:status response) 200))
      (is (= (get-in response [:headers "Content-Type"]) "application/json; charset=utf-8"))
      (is (= (count (:body response)) (count (json/generate-string (treef/build-map)))))
      (treef/insert 10 11)))
    (let [response (app (request :get "/customers/"))]
      (is (= (:status response) 200))
      (is (= (get-in response [:headers "Content-Type"]) "application/json; charset=utf-8"))
      (is (= (count (:body response)) (count (json/generate-string (treef/build-map)))))
      (treef/insert 11 12))
    (let [response (app (request :get "/customers/"))]
      (is (= (:status response) 200))
      (is (= (get-in response [:headers "Content-Type"]) "application/json; charset=utf-8"))
      (is (= (count (:body response)) (count (json/generate-string (treef/build-map))))))

  (testing "post points endpoint"
    (let [response (app (-> (request :post "/invites/")
                            (body (json/generate-string {:customer "123"
                                                         :invited "1234"}))))]
      (is (= (:status response) 201))
      (is (= (get-in response [:headers "Content-Type"]) "application/json; charset=utf-8")))
    
    (let [response (app (request :get "/customers/"))]
      (is (= (:status response) 200))
      (is (= (get-in response [:headers "Content-Type"]) "application/json; charset=utf-8"))
      (is (= (count (:body response)) (count (json/generate-string (treef/build-map)))))))

  (testing "not-found route"
    (let [response (app (request :get "/bogus-route"))]
      (is (= (:status response) 404)))))