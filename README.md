# Reward System

## Solution

The reward system was implemented using clojure. To save the invites/invitees we use an atom with a map which has informations such as points, depth, confirmed flag, parent and key. When a new user is confirmed his parent receive points and this points propagate until the root of the tree. Points received are calculated by 0.5^k where k is the difference between the child depth and the depth of the item receiving points minus 1, so the points received by depth 1 when child of depth 2 is confirmed will be 1 (0.5^(2 - 1 - 1)). The system runs with a file as input to load tree data. Tests for the tree structure and endpoints were created.

## Input format

```
1 2
1 3 
3 4
2 4
4 5
4 6
```

## Setup

Install lein and use the commands of section run


## Run

### Webserver + read input file

```
$ cd folder-of-project
$ lein with-profile production run input.txt
```

### Tests

```
$ cd folder-of-project
$ lein with-profile dev test
```

## Endpoints

### GET /customers - List customers ordered by points

Response - 200:

```
[{
	"key": "1",
	"confirmed": true,
	"points": 0
}, {
	"key": "3",
	"confirmed": true,
	"points": 0
}]
```


## POST /invites - Create a new invite

Body:
```
{
	"customer":3,
	"invited":13
}
```

Response - 201:

```
{}
```