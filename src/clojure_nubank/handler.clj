(ns clojure-nubank.handler
  (:use compojure.core
        ring.middleware.json)
  (:require [compojure.handler :as handler]
            [ring.util.response :refer [response]]
            [compojure.route :as route]
            [clojure-nubank.treef :as treef ]))

; (defn wrap-log-request [handler]
;   (fn [req]
;     (println req)
;     (handler req)))

(defn show-customers [_] 
  {:status 200 :body (sort-by :points > (treef/build-map))})

(defn create-invitation [{invitation :body}]
  (let [
    customer (get invitation "customer")
    invited (get invitation "invited")
  ]
  (treef/insert customer invited))
  {:status 201 :body {}})

(defroutes app-routes
  (GET "/customers/" [] show-customers)
  (POST "/invites/" [] create-invitation)
  (route/not-found (response "{\"message\":\"Page not found\"}")))

(def app
  (-> app-routes
    ; wrap-log-request
    wrap-json-response
    wrap-json-body))