(ns clojure-nubank.treef)
(require '[clojure.math.numeric-tower :as math])

(def f (atom {}))

(defn has-parent [customer]
  (not (nil? (get-in @f [customer :parent]))))

(defn get-parent [customer]
  (get-in @f [customer :parent]))

(defn get-points [customer]
  (get-in @f [customer :points]))

(defn get-confirmed [customer]
  (get-in @f [customer :confirmed]))

(defn is-confirmed [customer]
  (get-in @f [customer :confirmed]))

(defn search [customer]
  (get @f customer))

(defn update-points [customer invited]
  (let [
      customer-depth (get-in @f [customer :depth])
      customer-parent  (get-in @f [customer :parent])
      invited-depth (get-in @f [invited :depth])
      current-points (get-in @f [customer :points])
      inc-points (math/expt 0.5 (- (- invited-depth 1) customer-depth))
    ]
    (swap! f update-in [customer :points] + inc-points)
    (if (not (nil? customer-parent))
      (update-points customer-parent invited))))
      

(defn confirm [customer invited]
  (if (not (is-confirmed customer))
    (if (has-parent customer)
      (update-points (get-parent customer) customer)))
  (swap! f assoc-in [customer :confirmed] true))
  

(defn insert [customer invited]
  (if (nil? (get @f customer))
    (swap! f assoc customer {:key customer :confirmed true :points 0 :parent nil :depth 0 }))
  (if (nil? (get @f invited))
    (let [
        inc-depth (inc (get-in @f [customer :depth]))
      ]
      (swap! f assoc invited {:key invited :confirmed false :points 0 :parent customer :depth inc-depth})
      (if (has-parent customer)
        (confirm customer invited)))
      (confirm  customer invited)))

(defn build-map []
  (let [
      values (vals @f)
    ]
    (if (nil? values)
      []
      values)))

(defn print-tree [] 
  (println@f))

(defn reset []
  (reset! f {}))

  