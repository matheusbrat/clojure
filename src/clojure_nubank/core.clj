(ns clojure-nubank.core
  (:require [clojure-nubank.treef :as treef ]
            [clojure-nubank.handler :as handler]
            [clojure.java.io :as io]
            [ring.adapter.jetty :as jetty]
            [clojure.string :as str])
  (:gen-class))

(defn parse-line [line] 
  (let [[customer invited] (str/split line #" ")]
      (treef/insert (read-string customer) (read-string invited))))

(defn load-content [filename]
    (with-open [rdr (io/reader filename)]
      (doseq [line (line-seq rdr)]
        (parse-line line))))

(defn -main [& args]
  (load-content (first args))
  (treef/print-tree)
  (jetty/run-jetty handler/app {:port 3000}))
